
from setuptools import setup

setup(
    name = "pymymailru",
    version = "0.1",
    description = 'Mail.ru apps platform api connector',
    author = '',
    author_email = '',
    url = 'https://bitbucket.org/rebranch/',
    packages = ['pymymailru'],
    license = "BSD",
    scripts=[],
)
